#include<stdio.h>
#include<math.h>
struct point
{
	float x;
	float y;
};
typedef struct point Point;
Point input()
{
	Point a;
	printf("enter x and y co-ordinates:\n");
	scanf("%f%f",&a.x,&a.y);
	return a;
}
float distance(Point p1,Point p2)
{
	float d;
	d=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
	return d;
}
	void output(Point p1,Point p2,float d)
{
	printf("distance between %f,%f and %f,%f is %f",p1.x,p1.y,p2.x,p2.y,d);
}
int main()
{
	Point p1,p2;
	float d;
	p1=input();
	p2=input();
	d=distance (p1,p2);
	output(p1,p2,d);
	return 0;
}